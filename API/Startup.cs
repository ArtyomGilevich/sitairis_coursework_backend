﻿using System;
using System.Text;
using API.Authorization;
using API.Common;
using API.MappingProfiles;
using Autofac;
using AutoMapper;
using DAL;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("HELLO THERE. THIS IS MY SECRET KEEEEEEEEEEEEEy")),

                        ValidateIssuer = true,
                        ValidIssuer = "issuer",

                        ValidateAudience = true,
                        ValidAudience = "audience",

                        ValidateLifetime = true, // validate the expiration and not before values in the token

                        ClockSkew = TimeSpan.FromMinutes(5) // 5 minute tolerance for the expiration date
                    };
                });
            services.AddAuthentication();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddAuthorization(options =>
            {
                options.AddPolicy(
                    CommonConstansts.AdminRoleActionPolicyName, policy => policy.Requirements.Add(new AdminRoleRequirment()));
            });
            services.AddScoped<IAuthorizationHandler, AdminRoleHandler>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddDbContext<RailwaysDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("RailwaysDb")));
            services.AddAutoMapper(typeof(RailwayStationProfile).Assembly);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseAuthentication();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller}/{action}");
            });
        }

        public void ConfigureContainer(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<RailwaysDbContext>().AsSelf();
        }
    }
}
