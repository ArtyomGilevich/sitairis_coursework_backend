﻿namespace API.DTOs
{
    public class TrainCarriageWithTicketsInfoDto
    {
        public int CarriageId { get; set; }

        public int CarriageNumber { get; set; }

        public int SeatsAvailable { get; set; }
    }
}