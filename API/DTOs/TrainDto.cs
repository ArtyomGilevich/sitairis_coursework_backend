﻿using System;

namespace API.DTOs
{
    public sealed class TrainDto
    {
        public int Id { get; set; }

        public string Number { get; set; }

        public string StartStation { get; set; }

        public DateTime DepartureTime { get; set; }

        public string EndStation { get; set; }

        public DateTime ArrivalTime { get; set; }

        public int SeatsAvailable { get; set; }
    }
}