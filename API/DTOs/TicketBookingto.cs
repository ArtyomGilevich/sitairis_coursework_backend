﻿using DAL.Models;

namespace API.DTOs
{
    public class TicketBookingDto
    {
        public int TicketId { get; set; }

        public PassengerData PassengerData { get; set; }
    }
}