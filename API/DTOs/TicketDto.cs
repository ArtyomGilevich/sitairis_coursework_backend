﻿namespace API.DTOs
{
    public class TicketDto
    {
        public int TicketId { get; set; }

        public string SeatNumber { get; set; }

        public decimal Price { get; set; }

        public bool IsAvailable { get; set; }
    }
}