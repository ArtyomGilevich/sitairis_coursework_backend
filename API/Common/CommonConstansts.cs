﻿namespace API.Common
{
    internal static class CommonConstansts
    {
        public const string AdminRoleActionPolicyName = "AdminRoleAction";

        public const string UserIdClaimName = "userid";
    }
}