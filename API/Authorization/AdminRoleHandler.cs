﻿using System.Threading.Tasks;
using API.Common;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;

namespace API.Authorization
{
    public class AdminRoleHandler : AuthorizationHandler<AdminRoleRequirment>
    {
        private readonly IRepository<User> usersRepository;

        public AdminRoleHandler(IRepository<User> usersRepository)
        {
            this.usersRepository = usersRepository;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AdminRoleRequirment requirement)
        {
            if (context.User.HasClaim(claim =>
                claim.Type == CommonConstansts.UserIdClaimName && IsUserHasAdminRole(int.Parse(claim.Value))))
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail();
            }

            return Task.CompletedTask;
        }

        private bool IsUserHasAdminRole(int userId)
        {
            return usersRepository.FirstOrDefault(user => user.Id == userId)?.Role == Role.Admin;
        }
    }
}
