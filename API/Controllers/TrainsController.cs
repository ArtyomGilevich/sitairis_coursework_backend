﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.DTOs;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TrainsController : ControllerBase
    {
        private readonly IRepository<Train> trainsRepository;

        private readonly IMapper mapper;

        public TrainsController(IRepository<Train> trainsRepository, IMapper mapper)
        {
            this.trainsRepository = trainsRepository;
            this.mapper = mapper;
        }

        [Route("all")]
        [HttpGet]
        public IActionResult GetAllTrains()
        {
            List<Train> trains = GetAllTrainsQuery().ToList();
            return new JsonResult(mapper.Map<IEnumerable<TrainDto>>(trains));
        }

        [Route("")]
        [HttpGet]
        public IActionResult GetTrains(int startStationId, int endStationId, DateTime departureDate)
        {
            IQueryable<Train> trains = GetAllTrainsQuery();

            trains = trains.Where(t => t.TrainStops.FirstOrDefault(ts => ts.RailwayStationId == startStationId).Order <
                t.TrainStops.FirstOrDefault(ts =>  ts.RailwayStationId == endStationId).Order);

            trains = trains.Where(t =>
                t.TrainStops.FirstOrDefault(ts => ts.RailwayStationId == startStationId).DepartureTime < departureDate.AddDays(1) &&
                    t.TrainStops.FirstOrDefault(ts => ts.RailwayStationId == startStationId).DepartureTime >= departureDate);

            return new JsonResult(mapper.Map<IEnumerable<TrainDto>>(trains));
        }

        private IQueryable<Train> GetAllTrainsQuery()
        {
            return trainsRepository.Where().Include("TrainStops.RailwayStation").Include("Tickets").AsNoTracking();
        }
    }
}