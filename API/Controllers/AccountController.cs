﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using API.Common;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace API.Controllers
{
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IRepository<User> repository;

        public AccountController(IRepository<User> repository)
        {
            this.repository = repository;
        }

        [Route("/token")]
        [HttpPost]
        public IActionResult Login(string login, string password)
        {
            User user = GetUserData(login, EncryptPassword(password));
            if (user == null)
                return BadRequest();
            return new ObjectResult(GetJwtToken(login, user.Id));
        }

        [Route("/register")]
        [HttpPost]
        public IActionResult Register(string login, string password)
        {
            if (repository.Where().Any(x => x.Login.Equals(login, StringComparison.OrdinalIgnoreCase)))
            {
                return BadRequest("Пользователь с таким логином уже существует");
            }

            repository.Add(new User
            {
                Login = login,
                Password = EncryptPassword(password),
                Role = Role.User
            });

            return Ok();
        }

        private string GetJwtToken(string username, int userId)
        {
            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Exp, $"{new DateTimeOffset(DateTime.Now.AddDays(1)).ToUnixTimeSeconds()}"),
                new Claim(JwtRegisteredClaimNames.Nbf, $"{new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()}"),
                new Claim(CommonConstansts.UserIdClaimName, userId.ToString())
            };

            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("HELLO THERE. THIS IS MY SECRET KEEEEEEEEEEEEEy"));

            var token = new JwtSecurityToken(
                claims: claims,
                audience: "audience",
                issuer: "issuer",
                notBefore: DateTime.Now,
                expires: DateTime.Now.AddHours(1),
                signingCredentials: new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private User GetUserData(string login, string password)
        {
            return repository.FirstOrDefault(x => x.Login == login && x.Password == password);
        }

        private string EncryptPassword(string passwordToEncrypt)
        {
            return passwordToEncrypt;
        }
    }
}