﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.Common;
using API.DTOs;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class StationsController : ControllerBase
    {
        private const string StationAlreadyExistsMessage = "Станция с таким именем уже существует";

        private readonly IRepository<RailwayStation> repository;

        private readonly IMapper mapper;

        public StationsController(IRepository<RailwayStation> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        [Route("")]
        [HttpGet]
        public IActionResult GetAllStations()
        {
            List<RailwayStation> stations = repository.Where().AsNoTracking().ToList();
            return new JsonResult(mapper.Map<IEnumerable<RailwayStationDto>>(stations));
        }
        
        [Authorize(Policy = CommonConstansts.AdminRoleActionPolicyName)]
        [Route("")]
        [HttpPost]
        public IActionResult CreateStation(RailwayStationDto stationToCreate)
        {
            if (repository.Where().Any(station =>
                station.Name.Equals(stationToCreate.Name, StringComparison.OrdinalIgnoreCase)))
            {
                return BadRequest(StationAlreadyExistsMessage);
            }

            // the line below prevents attempt to explicitly set ID for entity: otherwise, an error will occur
            stationToCreate.Id = 0; 

            repository.Add(mapper.Map<RailwayStation>(stationToCreate));

            return Ok();
        }

        [Route("{stationId}")]
        [HttpGet]
        public IActionResult GetStation(int stationId)
        {
            RailwayStation station = repository.FirstOrDefault(x => x.Id == stationId);
            if (station == null)
            {
                return NotFound();
            }
            return new JsonResult(station);
        }

        [Authorize(Policy = CommonConstansts.AdminRoleActionPolicyName)]
        [Route("{stationId}")]
        [HttpPatch]
        public IActionResult UpdateStation(int stationId, RailwayStationDto newStation)
        {
            // stationId must not be filled in request body
            if (newStation.Id != default(int))
            {
                return BadRequest();
            }

            IQueryable<RailwayStation> allStations = repository.Where();

            RailwayStation oldStation = allStations.FirstOrDefault(station => station.Id == stationId);
            if (oldStation == null)
            {
                return NotFound();
            }

            if (allStations.Any(
                station => station.Id != stationId && station.Name.Equals(newStation.Name, StringComparison.OrdinalIgnoreCase)))
            {
                return BadRequest(StationAlreadyExistsMessage);
            }

            repository.Update(mapper.Map<RailwayStation>(newStation));
            return Ok();
        }

        [Authorize(Policy = CommonConstansts.AdminRoleActionPolicyName)]
        [Route("{stationId}")]
        [HttpDelete]
        public IActionResult DeleteStation(int stationId)
        {
            RailwayStation stationToDelete = repository.FirstOrDefault(station => station.Id == stationId);
            if (stationToDelete == null)
            {
                return NotFound();
            }
            repository.Delete(stationToDelete);
            return Ok();
        }
    }
}