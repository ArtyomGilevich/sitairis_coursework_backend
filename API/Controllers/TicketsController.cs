﻿using System.Collections.Generic;
using System.Linq;
using API.DTOs;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TicketsController : ControllerBase
    {
        private readonly IRepository<TrainCarriage> trainCarriagesRepository;

        private readonly IRepository<PassengerData> passengerDataRepository;

        private readonly IRepository<TicketReservation> ticketReservationsRepository;

        private readonly IRepository<Ticket> ticketsRepository;

        public TicketsController(
            IRepository<TrainCarriage> trainCarriagesRepository,
            IRepository<PassengerData> passengerDataRepository,
            IRepository<TicketReservation> ticketReservationsRepository,
            IRepository<Ticket> ticketsRepository)
        {
            this.trainCarriagesRepository = trainCarriagesRepository;
            this.passengerDataRepository = passengerDataRepository;
            this.ticketReservationsRepository = ticketReservationsRepository;
            this.ticketsRepository = ticketsRepository;
        }

        [Route("carriages")]
        [HttpGet]
        public IActionResult GetCarriagesWithAvailableTickets(int trainId)
        {
            IEnumerable<TrainCarriageWithTicketsInfoDto> carriages = trainCarriagesRepository
                .Where(t => t.TrainId == trainId)
                .Include("Tickets")
                .Select(tc => new TrainCarriageWithTicketsInfoDto
                {
                    CarriageId = tc.Id,
                    CarriageNumber = tc.Number,
                    SeatsAvailable = tc.Tickets.Count(t => t.TicketReservationId == null && t.TicketSaleId == null)
                })
                .Where(t => t.SeatsAvailable > 0);

            return new JsonResult(carriages);
        }

        [Route("carriages/{carriageId}")]
        [HttpGet]
        public IActionResult GetCarriageAvailableTickets(int carriageId)
        {
            TrainCarriage carriage = trainCarriagesRepository.FirstOrDefault(c => c.Id == carriageId, "Tickets");
            if (carriage == null)
            {
                return BadRequest("Не удалось найти вагон");
            }

            IEnumerable<TicketDto> tickets = carriage.Tickets.Select(t => new TicketDto
            {
                TicketId = t.Id,
                Price = t.Price,
                SeatNumber = t.Seat,
                IsAvailable = t.TicketReservationId == null && t.TicketSaleId == null
            });
            return new JsonResult(tickets);
        }

        [HttpPost]
        public IActionResult BookATicket(TicketBookingDto ticketBookingDto)
        {
            Ticket ticket = ticketsRepository.FirstOrDefault(x => x.Id == ticketBookingDto.TicketId);

            if (ticket == null)
            {
                return BadRequest("Место не найдено в системе");
            }

            if (ticket.PassengerDataId != null)
            {
                return BadRequest("Место уже занято");
            }

            if (ticketBookingDto.PassengerData.PassengerDataId == default(int))
            {
                passengerDataRepository.Add(ticketBookingDto.PassengerData);
            }

            ticketReservationsRepository.Add(new TicketReservation
            {
                TicketId = ticketBookingDto.TicketId
            });

            ticket.PassengerDataId = ticketBookingDto.PassengerData.PassengerDataId;
            ticketsRepository.Update(ticket);

            return Ok();
        }
    }
}