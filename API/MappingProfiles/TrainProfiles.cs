﻿using System.Collections.Generic;
using System.Linq;
using API.DTOs;
using AutoMapper;
using DAL.Models;

namespace API.MappingProfiles
{
    public class TrainProfiles : Profile
    {
        public TrainProfiles()
        {
            CreateMap<TrainDto, Train>(MemberList.Destination)
                .ForMember(x => x.Id, opt => opt.MapFrom(y => y.Id))
                .ForMember(x => x.Number, opt => opt.MapFrom(y => y.Number))
                .ForMember(x => x.Tickets, opt => opt.Ignore())
                .ForMember(x => x.TrainCarriages, opt => opt.Ignore())
                .ForMember(x => x.TrainStops, opt => opt.Ignore());
            CreateMap<Train, TrainDto>(MemberList.Destination)
                .BeforeMap((Train train, TrainDto trainDto) =>
                {
                    List<TrainStop> trainStops = train.TrainStops.OrderBy(ts => ts.Order).ToList();
                    TrainStop firstStop = trainStops.First();
                    TrainStop lastStop = trainStops.Last();

                    trainDto.StartStation = firstStop.RailwayStation.Name;
                    trainDto.EndStation = lastStop.RailwayStation.Name;

                    trainDto.DepartureTime = firstStop.DepartureTime.Value;
                    trainDto.ArrivalTime = lastStop.ArrivalTime.Value;

                    trainDto.SeatsAvailable =
                        train.Tickets.Where(t => t.TicketReservationId == null && t.TicketSaleId == null).Count();
                })
                .ForMember(x => x.Id, opt => opt.MapFrom(y => y.Id))
                .ForMember(x => x.Number, opt => opt.MapFrom(y => y.Number))
                .ForMember(x => x.StartStation, opt => opt.Ignore())
                .ForMember(x => x.DepartureTime, opt => opt.Ignore())
                .ForMember(x => x.EndStation, opt => opt.Ignore())
                .ForMember(x => x.ArrivalTime, opt => opt.Ignore())
                .ForMember(x => x.SeatsAvailable, opt => opt.Ignore());
        }
    }
}