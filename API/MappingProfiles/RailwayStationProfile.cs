﻿using API.DTOs;
using AutoMapper;
using DAL.Models;

namespace API.MappingProfiles
{
    public class RailwayStationProfile : Profile
    {
        public RailwayStationProfile()
        {
            CreateMap<RailwayStation, RailwayStationDto>(MemberList.Destination)
                .ForMember(x => x.Id, opt => opt.MapFrom(y => y.Id))
                .ForMember(x => x.Name, opt => opt.MapFrom(y => y.Name));
            CreateMap<RailwayStationDto, RailwayStation>(MemberList.Destination)
                .ForMember(x => x.Id, opt => opt.MapFrom(y => y.Id))
                .ForMember(x => x.Name, opt => opt.MapFrom(y => y.Name))
                .ForMember(x => x.TrainStops, opt => opt.Ignore());
        }
    }
}