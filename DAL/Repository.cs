﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly RailwaysDbContext context;

        private readonly DbSet<TEntity> dbSet;

        public Repository(RailwaysDbContext context)
        {
            this.context = context;

            dbSet = context.Set<TEntity>();
        }

        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate = null)
        {
            return predicate != null ? dbSet.Where(predicate) : dbSet;
        }

        public void Add(TEntity entity)
        {
            dbSet.Add(entity);
            context.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            dbSet.Update(entity);
            context.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            dbSet.Remove(entity);
            context.SaveChanges();
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate = null, params string[] includes)
        {
            IQueryable<TEntity> query = dbSet;
            if (includes != null)
            {
                foreach (string include in includes)
                {
                    query = dbSet.Include(include);
                }
            }
            if (predicate == null)
            {
                return query.FirstOrDefault();
            }
            return query.FirstOrDefault(predicate);
        }
    }
}
