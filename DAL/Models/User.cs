﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class User
    {
        public int Id { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public Role Role { get; set; }

        public IEnumerable<TicketReservation> TicketReservations { get; set; }

        public IEnumerable<TicketSale> TicketSales { get; set; }
    }
}
