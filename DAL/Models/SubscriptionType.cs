﻿namespace DAL.Models
{
    public enum SubscriptionType
    {
        NewTrainsToStation = 1,
        NewTrainsFromStation = 2
    }
}