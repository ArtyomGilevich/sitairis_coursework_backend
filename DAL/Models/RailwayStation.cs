﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class RailwayStation
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public IEnumerable<TrainStop> TrainStops { get; set; }
    }
}
