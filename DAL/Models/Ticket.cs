﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class Ticket
    {
        public int Id { get; set; }

        [Required]
        public string Seat { get; set; }

        public decimal Price { get; set; }

        public int? TicketSaleId { get; set; }

        public TicketSale TicketSale { get; set; }

        public int? TicketReservationId { get; set; }

        public TicketReservation TicketReservation { get; set; }

        public int TrainCarriageId { get; set; }

        public TrainCarriage TrainCarriage { get; set; }

        public int? PassengerDataId { get; set; }

        public PassengerData PassengerData { get; set; }
    }
}
