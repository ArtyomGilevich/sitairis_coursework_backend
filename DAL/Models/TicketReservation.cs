﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Models
{
    public class TicketReservation
    {
        public int TicketReservationId { get; set; }

        public int? UserId { get; set; }

        public User User { get; set; }

        public int TicketId { get; set; }

        [ForeignKey("TicketId")]
        public Ticket Ticket { get; set; }
    }
}
