﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class TrainCarriage
    {
        public int Id { get; set; }

        public int Number { get; set; }

        public int TrainId { get; set; }

        public Train Train { get; set; }

        public IEnumerable<Ticket> Tickets { get; set; }
    }
}
