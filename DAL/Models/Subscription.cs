﻿namespace DAL.Models
{
    public class Subscription
    {
        public int SubscriptionId { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public SubscriptionType SubscriptionType { get; set; }

        public int RailwayStationId { get; set; }

        public RailwayStation RailwayStation { get; set; }
    }
}