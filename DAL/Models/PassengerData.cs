﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class PassengerData
    {
        public int PassengerDataId { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string DocumentNumber { get; set; }

        public int? UserId { get; set; }

        public User User { get; set; }
    }
}
