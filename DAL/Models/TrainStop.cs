﻿using System;

namespace DAL.Models
{
    public class TrainStop
    {
        public int TrainId { get; set; }

        public Train Train { get; set; }

        public int RailwayStationId { get; set; }

        public RailwayStation RailwayStation { get; set; }

        // defines train stop's order for current Train
        public int Order { get; set; }

        // will be null if current station is the first route station
        public DateTime? DepartureTime { get; set; }

        // will be null if current station is the last route station
        public DateTime? ArrivalTime { get; set; }
    }
}