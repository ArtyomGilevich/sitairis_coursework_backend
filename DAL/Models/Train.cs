﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class Train
    {
        public int Id { get; set; }

        [Required]
        public string Number { get; set; }

        public IEnumerable<TrainStop> TrainStops { get; set; }

        public IEnumerable<TrainCarriage> TrainCarriages { get; set; }

        public IEnumerable<Ticket> Tickets { get; set; }
    }
}