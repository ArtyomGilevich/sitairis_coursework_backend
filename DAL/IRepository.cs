﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace DAL
{
    public interface IRepository<TEntity>
    {
        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate = null);

        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate = null, params string[] includes);

        void Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);
    }
}
