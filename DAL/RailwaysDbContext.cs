﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class RailwaysDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<RailwayStation> RailwayStations { get; set; }

        public DbSet<Train> Trains { get; set; }

        public DbSet<Ticket> Tickets { get; set; }

        public DbSet<TrainStop> TrainStops { get; set; }

        public DbSet<Subscription> Subscriptions { get; set; }

        public RailwaysDbContext()
        {

        }

        public RailwaysDbContext(DbContextOptions<RailwaysDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TrainStop>()
                .HasKey(t => new { t.TrainId, t.RailwayStationId });

            modelBuilder.Entity<TrainStop>()
                .HasOne(trainStop => trainStop.Train)
                .WithMany(train => train.TrainStops)
                .HasForeignKey(trainStop => trainStop.TrainId);

            modelBuilder.Entity<TrainStop>()
                .HasOne(trainStop => trainStop.RailwayStation)
                .WithMany(station => station.TrainStops)
                .HasForeignKey(trainStop => trainStop.RailwayStationId);

            modelBuilder.Entity<Ticket>()
                .HasOne(x => x.TicketReservation)
                .WithOne(x => x.Ticket)
                .HasForeignKey<TicketReservation>(x => x.TicketId);

            modelBuilder.Entity<Ticket>()
                .HasOne(x => x.TicketSale)
                .WithOne(x => x.Ticket)
                .HasForeignKey<TicketSale>(x => x.TicketId);

            modelBuilder.Entity<TicketReservation>()
                .HasOne(x => x.User)
                .WithMany(x => x.TicketReservations)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TicketSale>()
                .HasOne(x => x.User)
                .WithMany(x => x.TicketSales)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TrainStop>().HasIndex("TrainId", "Order").IsUnique();
        }
    }
}
