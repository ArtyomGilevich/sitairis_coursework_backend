﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class AddedSubscriptionRailwayStationId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RailwayStationId",
                table: "Subscriptions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_RailwayStationId",
                table: "Subscriptions",
                column: "RailwayStationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Subscriptions_RailwayStations_RailwayStationId",
                table: "Subscriptions",
                column: "RailwayStationId",
                principalTable: "RailwayStations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subscriptions_RailwayStations_RailwayStationId",
                table: "Subscriptions");

            migrationBuilder.DropIndex(
                name: "IX_Subscriptions_RailwayStationId",
                table: "Subscriptions");

            migrationBuilder.DropColumn(
                name: "RailwayStationId",
                table: "Subscriptions");
        }
    }
}
