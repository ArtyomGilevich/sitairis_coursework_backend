﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class AddedTrainStopOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "TrainStops",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TrainStops_TrainId_Order",
                table: "TrainStops",
                columns: new[] { "TrainId", "Order" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TrainStops_TrainId_Order",
                table: "TrainStops");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "TrainStops");
        }
    }
}
