﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class FixTrainStops : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TrainStop_RailwayStations_RailwayStationId",
                table: "TrainStop");

            migrationBuilder.DropForeignKey(
                name: "FK_TrainStop_Trains_TrainId",
                table: "TrainStop");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TrainStop",
                table: "TrainStop");

            migrationBuilder.RenameTable(
                name: "TrainStop",
                newName: "TrainStops");

            migrationBuilder.RenameIndex(
                name: "IX_TrainStop_RailwayStationId",
                table: "TrainStops",
                newName: "IX_TrainStops_RailwayStationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TrainStops",
                table: "TrainStops",
                columns: new[] { "TrainId", "RailwayStationId" });

            migrationBuilder.AddForeignKey(
                name: "FK_TrainStops_RailwayStations_RailwayStationId",
                table: "TrainStops",
                column: "RailwayStationId",
                principalTable: "RailwayStations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TrainStops_Trains_TrainId",
                table: "TrainStops",
                column: "TrainId",
                principalTable: "Trains",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TrainStops_RailwayStations_RailwayStationId",
                table: "TrainStops");

            migrationBuilder.DropForeignKey(
                name: "FK_TrainStops_Trains_TrainId",
                table: "TrainStops");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TrainStops",
                table: "TrainStops");

            migrationBuilder.RenameTable(
                name: "TrainStops",
                newName: "TrainStop");

            migrationBuilder.RenameIndex(
                name: "IX_TrainStops_RailwayStationId",
                table: "TrainStop",
                newName: "IX_TrainStop_RailwayStationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TrainStop",
                table: "TrainStop",
                columns: new[] { "TrainId", "RailwayStationId" });

            migrationBuilder.AddForeignKey(
                name: "FK_TrainStop_RailwayStations_RailwayStationId",
                table: "TrainStop",
                column: "RailwayStationId",
                principalTable: "RailwayStations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TrainStop_Trains_TrainId",
                table: "TrainStop",
                column: "TrainId",
                principalTable: "Trains",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
